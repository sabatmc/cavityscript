clear
echo "The script exe_nu starts now"
echo "Hello, $USER!"
sn=1
#loop on the Reynolds number 
for nu in 0.01  0.005  0.001
do
	cp -r cavity cavity${sn}
	cd cavity${sn}/constant
	sed -e "s/xxxxx/${nu}/" transportProperties.orig > transportProperties
	cd ../
	echo "Running simulation ${sn} with nu=${nu}"
	blockMesh >logMesh
	icoFoam >logSim
	foamToVTK >logPP

	sn=$(( $sn + 1 ))
	cd ../
done
